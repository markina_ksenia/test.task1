package test.task.organization.rep;

import org.springframework.data.jpa.repository.JpaRepository;
import test.task.organization.model.Organization;

import java.util.Optional;

public interface OrganizationRepozitory extends JpaRepository<Organization, Long> {
    void deleteOrganizationById(Long id);
    Optional<Organization> findOrganizationById(Long id);
}

