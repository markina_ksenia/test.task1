package test.task.organization.exception;

public class OrganzationNotFoundException extends RuntimeException{
    public OrganzationNotFoundException(String message) {
        super(message);
    }
}
