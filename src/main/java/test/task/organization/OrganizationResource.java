package test.task.organization;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.task.organization.model.Organization;
import test.task.organization.service.OrganizationService;

import java.util.List;

@RestController
@RequestMapping("/organization")
public class OrganizationResource {
    private final OrganizationService organizationService;

    public OrganizationResource(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Organization>> getAllOrganization () {
        List<Organization> organizations = organizationService.findAllOrganization();
        return new ResponseEntity<>(organizations, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Organization> getOrganizationById (@PathVariable("id") Long id) {
        Organization organization = organizationService.findOrganizationById(id);
        return new ResponseEntity<>(organization, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Organization> addOrganization(@RequestBody Organization organization) {
        Organization newOrganization =organizationService.addOrganization(organization);
        return new ResponseEntity<>(newOrganization, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Organization> updateOrganization(@RequestBody Organization organization) {
        Organization updateOrganization =organizationService.addOrganization(organization);
        return new ResponseEntity<>(updateOrganization, HttpStatus.OK);
    }

    @DeleteMapping ("/delete/{id}")
    public ResponseEntity<?> deleteOrganization(@PathVariable("id") Long id) {
        organizationService.deleteOrganization(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
