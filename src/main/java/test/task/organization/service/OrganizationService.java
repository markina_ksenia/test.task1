package test.task.organization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.task.organization.exception.OrganzationNotFoundException;
import test.task.organization.model.Organization;
import test.task.organization.rep.OrganizationRepozitory;

import java.util.List;

@Service
public class OrganizationService {
    private final OrganizationRepozitory organizationRepozitory;

    @Autowired
    public OrganizationService(OrganizationRepozitory organizationRepozitory) {
        this.organizationRepozitory = organizationRepozitory;
    }

    public Organization addOrganization (Organization organization) {
        return organizationRepozitory.save(organization);
    }

    public List<Organization> findAllOrganization() {
        return organizationRepozitory.findAll();
    }

    public Organization updateOrganization(Organization organization) {
        return organizationRepozitory.save(organization);
    }

    public Organization findOrganizationById(Long id) {
        return organizationRepozitory.findOrganizationById(id).orElseThrow(() -> new OrganzationNotFoundException("Organization by id" + id + "was not found"));
    }

    public void deleteOrganization(Long id) {
        organizationRepozitory.deleteOrganizationById(id);
    }
}
