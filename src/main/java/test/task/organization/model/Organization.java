package test.task.organization.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Organization implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String org_name;
    private String fiz_adress;
    private String legal_adress;
    private String supervisor;

    public Organization(Long id, String org_name, String fiz_adress, String legal_adress, String supervisor) {
        this.id = id;
        this.org_name = org_name;
        this.fiz_adress = fiz_adress;
        this.legal_adress = legal_adress;
        this.supervisor = supervisor;
    }

    public Long getId() {
        return id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public String getFiz_adress() {
        return fiz_adress;
    }

    public String getLegal_adress() {
        return legal_adress;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public void setFiz_adress(String fiz_adress) {
        this.fiz_adress = fiz_adress;
    }

    public void setLegal_adress(String legal_adress) {
        this.legal_adress = legal_adress;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "id=" + id +
                ", org_name='" + org_name + '\'' +
                ", fiz_adress='" + fiz_adress + '\'' +
                ", legal_adress='" + legal_adress + '\'' +
                ", supervisor='" + supervisor + '\'' +
                '}';
    }
}
